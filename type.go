package main

type ComplexStruct struct {
	Int      int
	MyStruct MyStruct
	Anon     Anon
}

type Anon struct {
	Nested Nested
}

type Nested struct {
	MyStruct MyStruct
}

type MyStruct struct {
	Int64     int64
	StringPtr *string
}

func (this *ComplexStruct) Equal(that *ComplexStruct) bool {
	return deriveEqualComplexStruct(this, that)
}
