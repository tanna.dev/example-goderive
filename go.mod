module gitlab.com/tanna.dev/example-goderive

go 1.20

require github.com/awalterschulze/goderive v0.0.0-20230228170144-9d8b5e1d9113

require (
	github.com/kisielk/gotool v1.0.0 // indirect
	golang.org/x/sys v0.5.0 // indirect
	golang.org/x/tools v0.6.0 // indirect
)
