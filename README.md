# Example of using `goderive` to generate an `Equal` method for a complex struct setup in Go

A sample repo go to alongside [_Generating `Equal` methods for Go structs with `goderive`_](https://www.jvt.me/posts/2023/03/27/go-generate-equal-goderive/).
