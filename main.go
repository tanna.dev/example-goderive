package main

import "fmt"

func main() {
	this := ComplexStruct{
		Int: 0,
		MyStruct: MyStruct{
			Int64:     -1,
			StringPtr: ptr("foo"),
		},
		Anon: Anon{
			Nested: Nested{
				MyStruct: MyStruct{
					Int64:     0,
					StringPtr: nil,
				},
			},
		},
	}

	that := ComplexStruct{
		Int: 0,
		MyStruct: MyStruct{
			Int64:     -1,
			StringPtr: ptr("foo"),
		},
		Anon: Anon{
			Nested: Nested{
				MyStruct: MyStruct{
					Int64:     0,
					StringPtr: nil,
				},
			},
		},
	}

	fmt.Printf("this.Equal(that): %v\n", this.Equal(&that))

	that.MyStruct.StringPtr = nil

	fmt.Printf("this.Equal(that): %v\n", this.Equal(&that))

	that.MyStruct.StringPtr = this.MyStruct.StringPtr

	fmt.Printf("this.Equal(that): %v\n", this.Equal(&that))

	this.Anon.Nested.MyStruct.StringPtr = ptr("foo bar baz")

	fmt.Printf("this.Equal(that): %v\n", this.Equal(&that))
}

func ptr[T any](t T) *T {
	return &t
}
